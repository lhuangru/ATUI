#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <ephem.h>

int main(int argc, char *argv[])
{

  double second;
  double day,jd,arg,t,dt,jde,mjdtai,st,ss,ss2,tmp;
  double bt,LP,D,M,MP,F,a1,a2,a3,c0,c1,E,OM,P;
  double multl,multr,multb,suml,sumr,sumb;
  double sv,svp,svpp,svppp,su,sup,supp,suppp,sr,srp,srpp,srppp;
  double longitude,latitude,range;
  double W,tana,anga,incl,lambda,beta,liblon,liblat,psi,eps,obliq;
  double k1,k2,rho,tau,sigma,pliblon,pliblat;
  double alpha,delta,hourang,azimuth,elev,refrac;
  double x,rhost,rhoct,paral,da,topodelt,topora;
  double temp,press,ref;
  int i,month,intday,year,jan=1,epochyear,hour,minute,hh,mm,hh2,mm2;
  int m1,m2,m3,m4;
  long int ampl,ampr,ampb;
  char str[80];
  char s1[10],s2[10],s3[10],s4[10],s5[10],s6[10];
  FILE *fp;

  struct tm *tp;
  time_t timenow;

  if (argc < 7) {
    timenow = time(NULL);
    tp = gmtime(&timenow);
    month = (*tp).tm_mon+1;
    intday = (*tp).tm_mday;
    year = (*tp).tm_year + 1900;
    hour = (*tp).tm_hour;
    minute = (*tp).tm_min;
    second = (double) (*tp).tm_sec;
  } else {
    sscanf(argv[1],"%d",&month);
    sscanf(argv[2],"%d",&intday);
    sscanf(argv[3],"%d",&year);
    sscanf(argv[4],"%d",&hour);
    sscanf(argv[5],"%d",&minute);
    sscanf(argv[6],"%lf",&second);
  }

  day = intday + (hour + (minute + second/60.0)/60.0)/24.0;
  jd = julianday(&month,&day,&year);

  dt = dyntime(&jd);
  jde = jd + dt/86400.0;
  mjdtai = (jd + (dt - 32.184)/86400.0 - 2400000.5)*86400.0;

  bt = (jde - 2451545.0)/36525.0;

/*  Arguments from Meeus, reportedly from Chapront (1998) French publ.	*/
/*  LP = 218.3164477 + (481267.88123421 - (0.0015786 - (1.0/538841.0 -	*/
/*       (bt/65194000.0))*bt)*bt)*bt;					*/
/*  D = 297.8501921 + (445267.1114034 - (0.0018819 - (1.0/545868.0 -	*/
/*       (bt/113065000.0))*bt)*bt)*bt;					*/
/*  M = 357.5291092 + (35999.0502929 - (0.0001536 - (bt/24490000.0))*bt)*bt; */
/*									*/
/*  MP = 134.9633964 + (477198.8675055 + (0.0087414 + (1.0/69699.0 -	*/
/*       (bt/14712000.0))*bt)*bt)*bt;					*/
/*  F  = 93.2720950 + (483202.0175233 - (0.0036539 + (1.0/3526000.0 -	*/
/*       (bt/863310000.0))*bt)*bt)*bt;					*/
  OM = 125.0445479 - (1934.1362891 - (0.0020754 + (1.0/467441.0 -
       (bt/60616000.0))*bt)*bt)*bt;
  P = 83.3532465 + (4069.0137287 - (0.0103200 + (1.0/80053.0 -
       (bt/18999000.0))*bt)*bt)*bt;

/*  Arguments from Chapronts' Lunar Tables... (1991), Willmann-Bell	*/
/*  LP -> L; M -> l-prime; MP -> l; 					*/
  LP = 218.31665436 + (481267.88134240 - (0.00013268 - (1.0/538793.0 -
       (bt/65189000.0))*bt)*bt)*bt;
  D = 297.85020420 + (445267.11151675 - (0.0001630 - (1.0/545841.0 -
      (bt/113122000.0))*bt)*bt)*bt;
  M = 357.52910918 + (35999.05029094 - (0.0001536 - (bt/24390240.0))*bt)*bt;

  MP = 134.96341138 + (477198.86763133 + (0.0089970 + (1.0/69696.0 -
       (bt/14712000.0))*bt)*bt)*bt;
  F = 93.27209932 + (483202.01752731 - (0.0034029 + (1.0/3521000.0 -
       (bt/862070000.0))*bt)*bt)*bt;

  E = 1.0 - 0.002516*bt - 0.0000074*bt*bt;
  
  LP -= 360.0*floor(LP/360.0);
  D  -= 360.0*floor(D/360.0);
  M  -= 360.0*floor(M/360.0);
  MP -= 360.0*floor(MP/360.0);
  F  -= 360.0*floor(F/360.0);
  OM -= 360.0*floor(OM/360.0);
  P  -= 360.0*floor(P/360.0);
  a1 -= 360.0*floor(a1/360.0);
  a2 -= 360.0*floor(a2/360.0);
  a3 -= 360.0*floor(a3/360.0);

  printf("%02u/%02u/%4u at %02u:%02u:%07.4f\n",
         month,intday,year,hour,minute,second);
  printf("day= %lf\n",day);
  printf("JD = %lf; JDE= %lf; MJDTAI = %lf\n",jd,jde,mjdtai);
  printf("T  = %14.12lf\n",bt);
/*  printf("LP = %lf\n",LP);	*/
/*  printf("D  = %lf\n",D);	*/
/*  printf("M  = %lf\n",M);	*/
/*  printf("MP = %lf\n",MP);	*/
/*  printf("F  = %lf\n",F);	*/
/*  printf("OM = %lf\n",OM);	*/
/*  printf("P  = %lf\n",P);	*/
/*  printf("A1 = %lf\n",a1);	*/
/*  printf("A2 = %lf\n",a2);	*/
/*  printf("A3 = %lf\n",a3);	*/
/* printf("E  = %lf\n",E);	*/

  a1 *= DTR;
  a2 *= DTR;
  a3 *= DTR;

  sv = 0.0;
  svp = 0.0;
  svpp = 0.0;
  svppp = 0.0;
  su = 0.0;
  sup = 0.0;
  supp = 0.0;
  suppp = 0.0;
  sv = 0.0;
  svp = 0.0;
  svpp = 0.0;
  svppp = 0.0;

  fp = fopen("/Users/tmurphy/ephem/src/sv_sr.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld\t\t%ld",&m1,&m2,&m3,&m4,&ampl,&ampr);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    sv += (double) ampl * 0.00000001 * sin(arg);
    sr += (double) ampr * 0.0001 * cos(arg);
    fgets(str,80,fp);
  } 
  fclose(fp);

  fp = fopen("/Users/tmurphy/ephem/src/svp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t\t%lf\t\t%lf",&ampl,&c0,&c1);
    arg = DTR * fmod(c0 + c1*bt, 360.0);
    svp += (double) ampl * 0.00001 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  fp = fopen("/Users/tmurphy/ephem/src/svpp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t\t%lf\t\t%lf",&ampl,&c0,&c1);
    arg = DTR * fmod(c0 + c1*bt, 360.0);
    svpp += (double) ampl * 0.00001 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  fp = fopen("/Users/tmurphy/ephem/src/svppp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    svppp += (double) ampl * 0.01 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  fp = fopen("/Users/tmurphy/ephem/src/su.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampb);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    su += (double) ampb * 0.00000001 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  fp = fopen("/Users/tmurphy/ephem/src/sup.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t\t%lf\t\t%lf",&ampl,&c0,&c1);
    arg = DTR * fmod(c0 + c1*bt, 360.0);
    sup += (double) ampl * 0.00001 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  fp = fopen("/Users/tmurphy/ephem/src/supp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    supp += (double) ampl * 0.00001 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  fp = fopen("/Users/tmurphy/ephem/src/suppp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    suppp += (double) ampl * 0.01 * sin(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  fp = fopen("/Users/tmurphy/ephem/src/srp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t\t%lf\t\t%lf",&ampl,&c0,&c1);
    arg = DTR * fmod(c0 + c1*bt, 360.0);
    srp += (double) ampl * 0.0001 * cos(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  fp = fopen("/Users/tmurphy/ephem/src/srpp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    srpp += (double) ampl * 0.0001 * cos(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  fp = fopen("/Users/tmurphy/ephem/src/srppp.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%ld",&m1,&m2,&m3,&m4,&ampl);
    arg = (m1*D + m2*M + m3*MP + m4*F) * DTR;
    srppp += (double) ampl * 0.1 * cos(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  printf("sum of s_v terms is: %lf\n",sv);
  printf("sum of s_v' terms is: %lf\n",svp);
  printf("sum of s_v'' terms is: %lf\n",svpp);
  printf("sum of s_v''' terms is: %lf\n",svppp);
  printf("sum of s_u terms is: %lf\n",su);
  printf("sum of s_u' terms is: %lf\n",sup);
  printf("sum of s_u'' terms is: %lf\n",supp);
  printf("sum of s_u''' terms is: %lf\n",suppp);
  printf("sum of s_r terms is: %lf\n",sr);
  printf("sum of s_r' terms is: %lf\n",srp);
  printf("sum of s_r'' terms is: %lf\n",srpp);
  printf("sum of s_r''' terms is: %lf\n",srppp);

/*  printf("sum of long. terms is: %lf\n",suml);	*/
/*  printf("sum of lat.  terms is: %lf\n",sumb);	*/
/*  printf("sum of rad.  terms is: %lf\n",sumr);	*/

  longitude = LP + sv + 0.001*(svp + svpp*bt + 0.0001*svppp*bt*bt);
  latitude = su + 0.001*(sup + supp*bt + 0.0001*suppp*bt*bt);;
  range = 385000.57 + sr + srp + srpp*bt + 0.0001*srppp*bt*bt;

  printf("mean longitude, latitude is %lf, %lf\n",longitude,latitude);
  printf("range is %lf km\n",range);

  nutation(&jd,&psi,&eps,&obliq);

  printf("Nutation is: (%lf,%lf), obliquity is: %lf\n",psi,eps,obliq);

  incl = 1.54242 * DTR;
  lambda = (longitude  + psi/3600.0)* DTR;
  beta = latitude * DTR;
  W = lambda - OM * DTR;
  tana = (sin(W)*cos(beta)*cos(incl)-sin(beta)*sin(incl))/(cos(W)*cos(beta));
  liblon = atan(tana)*RTD - F;
  if (liblon >  20.0) liblon -= 180.0;
  if (liblon < -20.0) liblon += 180.0;
  anga = (liblon + F)*DTR;
  liblat = -asin(sin(W)*cos(beta)*sin(incl) + sin(beta)*cos(incl))*RTD;

  printf("Libration in long., lat. is: (%lf,%lf)\n",liblon,liblat);

  k1 = 119.75 + 131.849*bt;
  k2 = 72.56  +  20.186*bt;
  k1 -= 360.0*floor(k1/360.0);
  k2 -= 360.0*floor(k2/360.0);

  MP *= DTR;
  D  *= DTR;
  F  *= DTR;
  M  *= DTR;

  rho = -0.02752*cos(MP) - 0.02245*sin(F) + 0.00684*cos(MP-2*F);
  rho += -0.00293*cos(2*F) - 0.00085*cos(2*(F-D));
  rho += -0.00054*cos(MP-2*D) - 0.00020*sin(MP+F);
  rho += -0.00020*cos(MP+2*F) - 0.00020*cos(MP-F) + 0.00014*cos(MP+2*F-2*D);

  sigma = -0.02816*sin(MP) + 0.02244*cos(F) - 0.00682*sin(MP-2*F);
  sigma += -0.00279*sin(2*F) - 0.00083*sin(2*(F-D)) + 0.00069*sin(MP-2*D);
  sigma += 0.00040*cos(MP+F) - 0.00025*sin(2*MP) - 0.00023*sin(MP+2*F);
  sigma += 0.00020*cos(MP-F) + 0.00019*sin(MP-F) + 0.00013*sin(MP+2*(F-D));
  sigma -= 0.00010*cos(MP-3*F);

  tau = 0.02520*E*sin(M) + 0.00473*sin(2*(MP-F)) - 0.00467*sin(MP);
  tau += 0.00396*sin(k1*DTR) + 0.00276*sin(2*(MP-D)) + 0.00196*sin(OM*DTR);
  tau += -0.00183*cos(MP-F) + 0.00115*sin(MP-2*D) - 0.00096*sin(MP-D);
  tau += 0.00046*sin(2*(F-D)) - 0.00039*sin(MP-F) - 0.00032*sin(MP-M-D);
  tau += 0.00027*sin(2*MP - M - 2*D);
  tau += 0.00023*sin(k2*DTR) - 0.00014*sin(2*D) + 0.00014*cos(2*(MP-F));
  tau += -0.00012*sin(MP-2*F) - 0.00012*sin(2*MP) + 0.00011*sin(2*(MP-M-D));

/*  printf("rho = %lf; sigma = %lf; tau = %lf\n",rho,sigma,tau);	*/
/*  printf("A = %lf\n",anga*RTD);	*/

  pliblon = -tau + (rho*cos(anga) + sigma*sin(anga))*tan(liblat*DTR);
  pliblat = sigma*cos(anga) - rho*sin(anga);

  printf("Moon's physical librations are: (%lf,%lf)\n",pliblon,pliblat);

  st = siderial(&jd) + APOLONG;
  st += psi*cos((obliq + eps/3600.0)*DTR)/3600.0;
  st -= 360.0*floor(st/360.0);
  tmp = st/15.0;
  hms(&tmp,&hh,&mm,&ss);
  printf("Apparent Siderial time is: %12.8lf = %02u:%02u:%07.4lf\n",
           st,hh,mm,ss);

  ecliptoeq(&jd,&lambda,&beta,&alpha,&delta);

  tmp = alpha / 15.0;
  hms(&tmp,&hh,&mm,&ss);
  hms(&delta,&hh2,&mm2,&ss2);
  printf("\nApparent RA,dec: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n",
          alpha,delta,hh,mm,ss,hh2,mm2,ss2);

  topo(&st,&alpha,&delta,&range,&topodelt,&topora);

  hourang = st - topora;
  azel(&hourang,&topodelt,&azimuth,&elev);

  hms(&azimuth,&hh,&mm,&ss);
  hms(&elev,&hh2,&mm2,&ss2);
  printf("Azimuth, elevation: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n",
          azimuth,elev,hh,mm,ss,hh2,mm2,ss2);

  temp = 283.0;
  press = 717.0;
  ref = refraction(&elev,&temp,&press);
  tmp = elev + ref;
  hms(&tmp,&hh,&mm,&ss);
  if (fabs(ref) > 0.0) {
    printf("Elev = %lf = %d:%02u:%04.1lf, refracted by %lf arcsec\n",
            tmp,hh,mm,ss,ref*3600.0);
  }

}
