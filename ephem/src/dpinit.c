#include <stdio.h>
#include <stdlib.h>
#include <math>
#include "ephem"
#include "sgp4"
#include "deep"

void dpinit(double *eqsq, double *siniq, double *cosiq, double *rteqsq,
            double *a0, double *cosq2, double *sinom0, double *cosom0,
            double *bsq, double *lldot, double *omgdot, double *nodot,
            double *npp, double passarr[])
{

  double CK2,CK4,Q0MS2T,S;
  double m0,node0,omega0,e0,i0,n0,ndt20,ndd60,bstar,x,y,z,xdot,ydot,zdot;
  double epoch,ds50,*parr;

/* define some common block stuff not in sgp4.h			*/

  CK2 = XJ2 * AE * AE / 2.0;
  CK4 = -3.0 * XJ4 * pow(AE,4) / 8.0;
  Q0MS2T = pow((Q0 - S0)*AE/XKMPER,4.0);
  S = AE * (1.0 + S0/XKMPER);

/* My kluge method for passing common block of input element data	*/

  parr	= passarr;
  m0	= *parr++;
  node0	= *parr++;
  omega0= *parr++;
  e0	= *parr++;
  i0	= *parr++;
  n0	= *parr++;
  ndt20	= *parr++;
  ndd60	= *parr++;
  bstar	= *parr++;
  x	= *parr++;
  y	= *parr++;
  z	= *parr++;
  xdot	= *parr++;
  ydot	= *parr++;
  zdot	= *parr++;
  epoch	= *parr++;
  ds50	= *parr++;

  thgr = thetag(epoch);
  eq = e0;
  nq = npp;
  aqnv = 1.0/a0;
  qncl = i0;
  ma0 = m0;
  pidot = omgdot + nodot;
  sinq = sin(node0);
  cosq = cos(node0);
  omegaq = omega0;

/* Initialize Lunar/Solar terms					*/

  day = ds50 + 18261.5
  if (day != preep) {
    preep = day;
    nodce = 4.5236020 - 0.00092422029*day;
    stem = sin(nodce);
    ctem = cos(nodce);
    zcosil = 0.91375164 - 0.03568096*ctem;
    zsinil = sqrt(1.0 - zcosil*zcosil);
    zsinhl = 0.089683511*stem/zsinil;
    zcoshl = sqrt(1.0 - zsinhl*zsinhl);
    c = 4.7199672 + 0.22997150*day;
    gam = 5.8351514 + 0.0019443680*day;
    zmol = c - gam;
    zmol -= TWOPI*floor(zmol / TWOPI);
    zx = 0.39785416*stem/zsinil;
    zy = zcoshl*ctem + 0.91744867*zsinhl*stem;
    zx = atan(zx,zy);
    zcosgl = cos(zx);
    zsingl = sin(zx);
    zmos = 6.2565837 + 0.017201977*day;
    zmos -= TWOPI*floor(zmos / TWOPI);
  }

/* Do Solar Terms						*/

  ls = 0;
  savtsn = 1.0e20;
  zcosg = ZCOSGS;
  zsing = ZSINGS;
  zcosi = ZCOSIS;
  zsini = ZSINIS;
  zcosh = cosq;
  zsinh = sinq;
  cc = C1SS;
  zn = ZNS;
  ze = ZES;
  zmo = zmos;
  noi = 1.0/nq;
  ls = 30;	/* ASSIGN 30 TO LS was original FORTRAN ??	*/
  a1 = zcosg*zcosh + zsing*zcosi*zsinh;
  a3 = -zsing*zcosh + zcosg*zcosi*zsinh;
  a7 = -zcosg*zsinh + zsing*zcosi*zcosh;
  a8 = zsing*zsini;
  a9 = zsing*zsinh + zcosg*zcosi*zcosh;
  a10 = zcosg*zsini;
  a2 = cosiq*a7 + siniq*a8;
  a4 = cosiq*a9 + siniq*a10;
  a5 = -siniq*a7 + cosiq*a8;
  a6 = -siniq*a9 + cosiq*a10;

  x1 = a1*cosom0 + a2*sinom0;
  x2 = a3*cosom0 + a4*sinom0;
  x3 = -a1*sinom0 + a2*cosom0;
  x4 = -a3*sinom0 + a4*cosom0;
  x5 = a5*sinom0;
  x6 = a6*sinom0;
  x7 = a5*cosom0;
  x8 = a6*cosom0;

  z31 = 12.0*x1*x1 - 3.0*x3*x3;
  z32 = 24.0*x1*x2 - 6.0*x3*x4;
  z33 = 12.0*x2*x2 - 3.0*x4*x4;
  z1 = 3.0*(a1*a1 + a2*a2) + z31*eqsq;
  z2 = 6.0*(a1*a3 + a2*a4) + z32*eqsq;
  z3 = 3.0*(a3*a3 + a4*a4) + z33*eqsq;
  z11 = -6.0*a1*a5 + eqsq*(-24.0*x1*x7 - 6.0*x3*x5);
  z12 = -6.0*(a1*a6 + a3*a5) + eqsq*(-24.0*(x2*x7+x1*x8) - 6.0*(x3*x6+x4*x5));
  z13 = -6.0*a3*a6 + eqsq*(-24.0*x2*x8 - 6.0*x4*x6);
  z21 = 6.0*a2*a5 + eqsq*(24.0*x1*x5 - 6.0*x3*x7);
  z22 = 6.0*(a4*a5 + a2*a6) + eqsq*(24.0*(x2*x5+x1*x6) - 6.0*(x4*x7+x3*x8));
  z23 = 6.0*a4*a6 + eqsq*(24.0*x2*x6 - 6.0*x4*x8);
  z1 += z1 + bsq*z31;
  z2 += z2 + bsq*z32;
  z3 += z3 + bsq*z33;
  s3 = cc*noi;
  s2 = -0.5*s3/rteqsq;
  s4 = s3*rteqsq;
  s1 = -15.0*eq*s4;
  s5 = x1*x3 + x2*x4;
  s6 = x2*x3 + x1*x4;
  s7 = x2*x4 - x1*x3;
  se = s1*zn*s5;
  si = s2*zn*(z11 + z13);
  sl = -zn*s3*(z1 + z3 - 14.0 - 6.0*eqsq);
  sgh = s4*zn*(z31 + z33 - 6.0);
  sh = -zn*s2*(z21 + z23);
  if(qncl < 0.052359877) sh = 0.0;
  ee2 = 2.0*s1*s6;
  e3 = 2.0*s1*s7;
  i2 = 2.0*s2*z12;
  i3 = 2.0*s2*(z13 - z11);
  l2 = -2.0*s3*z2;
  l3 = -2.0*s3*(z3 - z1);
  l4 = -2.0*s3*(-21.0 - 9.0*eqsq)*ze;
  gh2 = 2.0*s4*z32;
  gh3 = 2.0*s4*(z33 - z31);
  gh4 = -18.0*s4*ze;
  h2 = -2.0*s2*z22;
  h3 = -2.0*s2*(z23 - z21);

/* Do Lunar Terms (GOTO LS, LS being 30, this line being 30)		*/

  sse = se;
  ssi = si;
  ssl = sl;
  ssh = sh/siniq;
  ssg = sgh - cosiq*ssh;
  se2 = ee2;
  si2 = i2;
  sl2 = l2;
  sgh2 = gh2;
  sh2 = h2;
  se3 = e3;
  si3 = i3;
  sl3 = l3;
  sgh3 = gh3;
  sh3 = h3;
  sl4 = l4;
  sgh4 = gh4;
  ls = 1;
  zcosg = zcosgl;
  zsing = zsingl;
  zcosi = zcosil;
  zsini = zsinil;
  zcosh = zcoshl*cosq + zsinhl*sinq;
  zsinh = sinq*zcoshl - cosq*zsinhl;
  zn = ZNL;
  cc = C1L;
  zmo = zmol;

/* do same thing as above, copied lines exactly (handled with GOTO orig.) */

  a1 = zcosg*zcosh + zsing*zcosi*zsinh;
  a3 = -zsing*zcosh + zcosg*zcosi*zsinh;
  a7 = -zcosg*zsinh + zsing*zcosi*zcosh;
  a8 = zsing*zsini;
  a9 = zsing*zsinh + zcosg*zcosi*zcosh;
  a10 = zcosg*zsini;
  a2 = cosiq*a7 + siniq*a8;
  a4 = cosiq*a9 + siniq*a10;
  a5 = -siniq*a7 + cosiq*a8;
  a6 = -siniq*a9 + cosiq*a10;

  x1 = a1*cosom0 + a2*sinom0;
  x2 = a3*cosom0 + a4*sinom0;
  x3 = -a1*sinom0 + a2*cosom0;
  x4 = -a3*sinom0 + a4*cosom0;
  x5 = a5*sinom0;
  x6 = a6*sinom0;
  x7 = a5*cosom0;
  x8 = a6*cosom0;

  z31 = 12.0*x1*x1 - 3.0*x3*x3;
  z32 = 24.0*x1*x2 - 6.0*x3*x4;
  z33 = 12.0*x2*x2 - 3.0*x4*x4;
  z1 = 3.0*(a1*a1 + a2*a2) + z31*eqsq;
  z2 = 6.0*(a1*a3 + a2*a4) + z32*eqsq;
  z3 = 3.0*(a3*a3 + a4*a4) + z33*eqsq;
  z11 = -6.0*a1*a5 + eqsq*(-24.0*x1*x7 - 6.0*x3*x5);
  z12 = -6.0*(a1*a6 + a3*a5) + eqsq*(-24.0*(x2*x7+x1*x8) - 6.0*(x3*x6+x4*x5));
  z13 = -6.0*a3*a6 + eqsq*(-24.0*x2*x8 - 6.0*x4*x6);
  z21 = 6.0*a2*a5 + eqsq*(24.0*x1*x5 - 6.0*x3*x7);
  z22 = 6.0*(a4*a5 + a2*a6) + eqsq*(24.0*(x2*x5+x1*x6) - 6.0*(x4*x7+x3*x8));
  z23 = 6.0*a4*a6 + eqsq*(24.0*x2*x6 - 6.0*x4*x8);
  z1 += z1 + bsq*z31;
  z2 += z2 + bsq*z32;
  z3 += z3 + bsq*z33;
  s3 = cc*noi;
  s2 = -0.5*s3/rteqsq;
  s4 = s3*rteqsq;
  s1 = -15.0*eq*s4;
  s5 = x1*x3 + x2*x4;
  s6 = x2*x3 + x1*x4;
  s7 = x2*x4 - x1*x3;
  se = s1*zn*s5;
  si = s2*zn*(z11 + z13);
  sl = -zn*s3*(z1 + z3 - 14.0 - 6.0*eqsq);
  sgh = s4*zn*(z31 + z33 - 6.0);
  sh = -zn*s2*(z21 + z23);
  if(qncl < 0.052359877) sh = 0.0;
  ee2 = 2.0*s1*s6;
  e3 = 2.0*s1*s7;
  i2 = 2.0*s2*z12;
  i3 = 2.0*s2*(z13 - z11);
  l2 = -2.0*s3*z2;
  l3 = -2.0*s3*(z3 - z1);
  l4 = -2.0*s3*(-21.0 - 9.0*eqsq)*ze;
  gh2 = 2.0*s4*z32;
  gh3 = 2.0*s4*(z33 - z31);
  gh4 = -18.0*s4*ze;
  h2 = -2.0*s2*z22;
  h3 = -2.0*s2*(z23 - z21);
  
/* now back to original flow (line 40 in orig)				*/

  sse += se;
  ssi += si;
  ssl += sl;
  ssg += sgh - sh*cosiq/siniq;
  ssh += sh/siniq;

/* Geopotential Resonance Initialization for 12-hour Orbits		*/

  iresfl = 0;
  isynfl = 0;
  if ((nq < 0.0052359877) && (nq > 0.0034906585)) {
    iresfl = 1;
    isynfl = 1;
    g200 = 1.0 + eqsq*(-2.5 + 0.8125*eqsq);
    g310 = 1.0 + 2.0*eqsq;
    g300 - 1.0 + eqsq*(-6.0 + 6.60937*eqsq);
    f330 = 1.0 + cosiq;
    f220 = 0.75*f330*f330;
    f311 = 0.9375*siniq*siniq*(1.0 + 3.0*cosiq) - 0.75*f330;
    f330 = 1.875*f330*f330*f330;
    del1 = 3.0*nq*nq*aqnv*aqnv;
    del2 = 2.0*del1*f220*g200*Q22;
    del3 = 3.0*del1*f330*g300*Q33*aqnv;
    del1 *= f311*g310*Q31*aqnv;
    fasx2 = 0.13130908;
    fasx4 = 2.8843198;
    fasx6 = 0.37448087;
    lam0 = ma0 + node0 + omega0 - thgr;
    bfact = lldot + pidot - thdt;
    bfact += ssl + ssg + ssh;
  } else {
    if ((nq < 0.00826) || (nq > 0.00924)) {
      printf("tripped up in nq hell\n");
      exit(1);
    }
    if (eq < 0.5) {
      printf("tripped up in eq hell\n");
      exit(1);
    }
    iresfl = 1;
    e0c = eq*eqsq;
    g201 = -0.306 - (eq - 0.64)*0.440;
    if (eq < 0.65) {
      g211 = 3.616 - 13.247*eq + 16.290*eqsq;
      g310 = -19.302 + 117.390*eq - 228.419*eqsq + 156.591*e0c;
      g322 = -18.9068 + 109.7927*eq - 214.6334*eqsq + 146.5816*e0c;
      g410 = -41.122 + 242.694*eq - 471.094*eqsq + 313.953*e0c;
      g422 = -146.407 + 841.880*eq - 1629.014*eqsq + 1083.435*e0c;
      g520 = -532.114 + 3017.977*eq - 5740.0*eqsq + 3708.276*e0c;
    } else {
      g211 = -72.099 + 331.819*eq - 508.738*eqsq + 266.724*e0c;
      g310 = -346.844 + 1582.851*eq - 2415.925*eqsq + 1246.113*e0c;
      g322 = -342.585 + 1554.908*eq - 2366.899*eqsq + 1215.972*e0c;
      g410 = -1052.797 + 4758.686*eq - 7193.992*eqsq + 3651.957*e0c;
      g422 = -3581.69 + 16178.11*eq - 24462.77*eqsq + 12422.52*e0c;
      g520 = 1464.74 - 4664.75*eq + 3763.64*eqsq;
      if (eq > 0.715) {
        g520 = -5149.66 + 29936.92*eq - 54087.36*eqsq + 31324.56*e0c;
      }
    }
    if (eq < 0.7) {
      g533 = -919.2277 + 4988.61*eq - 9064.77*eqsq + 5542.21*e0c;
      g521 = -822.71072 + 4568.6173*eq - 8491.4146*eqsq + 146349.42*e0c;
      g532 = -853.666 + 4690.25*eq - 8624.77*eqsq + 5341.4*e0c;
    } else {
      g533 = -37995.78 + 161616.52*eq - 229838.2*eqsq + 109377.94*e0c;
      g521 = -51752.104 + 218913.95*eq - 309468.16*eqsq + 146349.42*e0c;
      g532 = -40023.88 + 170470.89*eq - 242699.48*eqsq + 115605.82*e0c;
    }
    sini2 = siniq*siniq;
    f220 = 0.75*(1.0 + 2.0*cosiq*cosq2);
    f221 = 1.5*sini2;
    f321 = 1.875*siniq*(1.0 - 2.0*cosiq - 3.0*cosq2);
    f322 = -1.875*siniq*(1.0 + 2.0*cosiq - 3.0*cosq2);
    f441 = 35.0*sini2*f220;
    f442 = 39.3750*sini2*sini2;
    f522 = 9.84375*siniq*(sini2*(1.0 - 2.0*cosiq - 5.0*cosq2)
                          + 0.33333333*(-2.0 + 4.0*cosiq + 6.0*cosq2));
    f523 = siniq*(4.92187512*sini2*(-2.0 - 4.0*cosiq + 10.0*cosq2)
                  + 6.56250012*(1.0 + 2.0*cosiq - 3.0*cosq2));
    f542 = 29.53125*siniq*(2.0 - 8.0*cosiq + cosq2*(-12.0 + 8.0*cosiq
                           + 10.0*cosq2));
    f543 = 29.53125*siniq*(-2.0 - 8.0*cosiq + cosq2*(12.0 + 8.0*cosiq
                           - 10.0*cosq2));
    n02 = nq*nq;
    ainv2 = aqnv*aqnv;
    temp1 = 3.0*n02*ainv2;
    temp = temp1*ROOT22;
    d2201 = temp*f220*g201;
    d2211 = temp*f220*g211;
    temp1 *= aqnv;
    temp = temp1*ROOT32;
    d3210 = temp*f321*g310;
    d3222 = temp*f322*g322;
    temp1 *= aqnv;
    temp = 2.0*temp1*ROOT44;
    d4410 = temp*f441*g410;
    d4422 = temp*f442*g422;
    temp1 *= aqnv;
    temp = temp1*ROOT52;
    d5220 = temp*f522*g520;
    d5232 = temp*f523*g532;
    temp = 2.0*temp1*ROOT54;
    d5421 = temp*f542*g521;
    d5433 = temp*f543*g533;
    lam0 = ma0 + node0 + node0 - thgr - thgr;
    bfact = lldot + nodot + nodot - thdt - thdt;
    bfact += ssl + ssh + ssh;
  }
  xfact = bfact - nq;

/* initialize integrator						*/

  li = lam0;
  ni = nq;
  atime = 0.0;
  stepp = 720.0;
  stepn = -720.0;
  step2 = 259200.0;

}
